﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class AtmosphereGenerator
    {

        public AtmosphereGenerator() { }

        public Atmosphere Generate(PlanetType planetType, double SemiMajorAxis)
        {
            RandomSingleton random = RandomSingleton.Instance();
            Atmosphere atmosphere = new Atmosphere();

            switch (planetType)
            {
                case PlanetType.Terrestrial:
                    if (SemiMajorAxis > 0.4)
                    {
                        double COChance, OChance, ArChance;
                        COChance = random.NextDouble();
                        if (COChance < 0.95)
                            atmosphere.AddGas(Gas.CarbonDioxide, random.NextDouble(0, 50));
                        OChance = random.NextDouble();
                        if (OChance < 0.05)
                            atmosphere.AddGas(Gas.Oxygen, random.NextDouble(10, 25));
                        ArChance = random.NextDouble();
                        if (ArChance < 0.4)
                            atmosphere.AddGas(Gas.Argon, random.NextDouble(5));

                        atmosphere.AddGas(Gas.Nitrogen, 100.0 - atmosphere.GetGasPercentage());
                    }
                    break;
                case PlanetType.GiantGas:
                    atmosphere.AddGas(Gas.Hydrogen, random.NextDouble(60.0, 90.0));
                    atmosphere.AddGas(Gas.Helium, 100.0 - atmosphere.GetGasPercentage());
                    break;
                case PlanetType.GiantIce:
                    atmosphere.AddGas(Gas.Hydrogen, random.NextDouble(60.0, 90.0));
                    atmosphere.AddGas(Gas.Methane, random.NextDouble(1.0, 4.0));
                    atmosphere.AddGas(Gas.Helium, 100.0 - atmosphere.GetGasPercentage());
                    break;
            }

            return atmosphere;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    class Comet
    {
        public double Diameter { get; set; }

        public double Mass { get; set; }

        public double Density { get; set; }
    }
}

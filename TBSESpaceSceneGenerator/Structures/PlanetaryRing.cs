﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    public enum RingComposition
    {
        Ice,
        Rock,
        Count
    }

    class PlanetaryRing
    {
        public RingComposition Composition { get; set; }
    }
}
